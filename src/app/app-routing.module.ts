import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CatalogComponent} from './components/catalog/catalog.component';
import {OfferDetailsComponent} from './components/offer-details/offer-details.component';
import {UserAccountComponent} from './components/user-account/user-account.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {ShoppingCartComponent} from './components/shopping-cart/shopping-cart.component';
import {ShoppingCartDetailsComponent} from './components/shopping-cart-details/shopping-cart-details.component';
import {CheckoutsComponent} from './components/checkouts/checkouts.component';

const routes: Routes = [
  {path: '', redirectTo: '/catalog', pathMatch: 'full'},
  {path: 'offers/:id', component: OfferDetailsComponent},
  {path: 'catalog', component: CatalogComponent},
  {path: 'account', component: UserAccountComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'shopping-cart', component: ShoppingCartComponent},
  {path: 'shopping-cart/:id', component: ShoppingCartDetailsComponent},
  {path: 'checkouts/:id', component: CheckoutsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
