import {Component, OnInit} from '@angular/core';
import {User} from '../../shared/models/user';
import {AuthService} from '../../shared/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnInit {
  currentUser: User;

  constructor(private authService: AuthService, public router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log('user-account' + localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  }

  logOut() {
    console.log('log out');
    this.authService.logOut();
    this.currentUser = null;
    this.router.navigate(['/catalog']);

  }
}
