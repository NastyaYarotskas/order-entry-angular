import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../../shared/models/user';
import {Addres} from '../../shared/models/addres';
import {AuthService} from '../../shared/auth/auth.service';
import {OrderService} from '../../shared/order/order.service';
import {ActivatedRoute, Router} from '@angular/router';
import {OrderReal} from '../../shared/models/OrderReal';
import {timer} from 'rxjs';

@Component({
  selector: 'app-checkouts',
  templateUrl: './checkouts.component.html',
  styleUrls: ['./checkouts.component.css']
})
export class CheckoutsComponent implements OnInit {
  user: User;

  orderId: number;

  order: OrderReal;

  loading = false;

  isPaid = false;

  constructor(authService: AuthService,
              public orderService: OrderService,
              public route: ActivatedRoute,
              public router: Router) {
    this.user = authService.getCurrentUser();
    const id = this.route.snapshot.paramMap.get('id');
    this.orderService.getOrderById(+id).subscribe(data => {
      this.order = data as OrderReal;
    });

    timer(1000).subscribe(data => {
      this.loading = true;
    });
  }

  ngOnInit() {
  }

  updateUserDetails(form: NgForm) {
    const id = this.route.snapshot.paramMap.get('id');
    const data = form.value;

    data['emailId'] = this.user.email;
    data['userId'] = this.user.id;
    const products = [];

    const totalPrice = 0;

    data['products'] = products;

    data['totalPrice'] = totalPrice;

    data['shippingDate'] = Date.now();

    console.log(this.order.deliveryAddress);

    this.orderService.payForOrder(String(id));

    timer(1000).subscribe(() => {
      this.isPaid = true;
      this.router.navigate(['/catalog']);
    });
  }
}
