import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../shared/order/order.service';
import {Order} from '../../shared/models/order';
import {AuthService} from '../../shared/auth/auth.service';
import {timer} from 'rxjs';
import {OrderReal} from '../../shared/models/OrderReal';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  cartItems: OrderReal[];

  loading = false;

  constructor(public orderService: OrderService,
              public authService: AuthService) {
  }

  ngOnInit() {
    timer(1000).subscribe(val => {
      this.getCartItems();

    });

  }

  getCartItems() {
    const email = this.authService.getCurrentUser().email;
    console.log('email: ' + email);
    this.orderService.getOrdersByEmail(email).subscribe(
      data => {
        // this.cartItems = data as Order[];
        this.cartItems = data as OrderReal[];
        console.log('this.cartItems: ' + this.cartItems);
        this.loading = true;
      }
    );

    return this.cartItems;
  }
}
