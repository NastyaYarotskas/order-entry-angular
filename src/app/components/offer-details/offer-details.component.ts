import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OfferService} from '../../shared/offer/offer.service';
import {Offer} from '../../shared/models/offer';
import {OrderService} from '../../shared/order/order.service';
import {Order} from '../../shared/models/order';
import {variable} from '@angular/compiler/src/output/output_ast';
import {__await} from 'tslib';
import {timer} from 'rxjs';
import {AuthService} from '../../shared/auth/auth.service';

@Component({
  selector: 'app-offer-details',
  templateUrl: './offer-details.component.html',
  styleUrls: ['./offer-details.component.css']
})
export class OfferDetailsComponent implements OnInit {

  offer: Offer;
  loading = false;
  order: Order;
  quantity = 1;

  constructor(private route: ActivatedRoute,
              private offerService: OfferService,
              private orderService: OrderService,
              public authService: AuthService) {
  }

  ngOnInit() {
    this.getOffer();
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  incQuantity() {
    this.quantity++;
  }

  decQuantity() {
    if (this.quantity !== 1) {
      this.quantity--;
    }
  }

  getOffer(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.offerService.getOffer(id)
      .subscribe(offer => {
        this.offer = offer as Offer;
        this.loading = true;
      });
  }

  addToCart() {
    console.log('add to cart');

    const orderId = this.orderService.getOrderId;
    console.log('orderId: ' + orderId);
    if (orderId < 0) {
      const order = this.orderService.createOrder();
    }

    console.log('orderId: ' + orderId);
    timer(1000).subscribe(val => {
      console.log('else ');
      (this.orderService.addOrderItem(this.offer, this.quantity));
    });


  }
}
