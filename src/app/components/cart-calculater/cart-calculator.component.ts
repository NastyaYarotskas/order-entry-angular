import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {OrderItem} from '../../shared/models/order-item';
import {timer} from 'rxjs';

@Component({
  selector: 'app-cart-calculater',
  templateUrl: './cart-calculator.component.html',
  styleUrls: ['./cart-calculator.component.css']
})
export class CartCalculatorComponent implements OnInit, OnChanges {
  @Input() orderItems: OrderItem[];

  @Input() orderId: number;

  @Input() orderPaid: boolean;

  totalValue = 0;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const dataChanges: SimpleChange = changes.orderItems;

    const products: OrderItem[] = dataChanges.currentValue;

    this.totalValue = 0;
    products.forEach(product => {
      console.log(
        'Adding: ' + product.name + ' $ ' + product.price
      );
      this.totalValue += product.price * product.quantity;
    });
  }

  ngOnInit() {
  }

}
