import {Component, Input, OnInit} from '@angular/core';
import {OfferService} from '../../shared/offer/offer.service';
import {CategoryService} from '../../shared/category/category.service';
import {Category} from '../../shared/models/category';
import {Filter} from '../../shared/models/filter';

import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {ElementRef, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete} from '@angular/material';
import {Observable, timer} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  offers: Array<any>;

  categories: Array<any>;

  selectedCategory: Category;

  selectedTags: string;

  minPrice: string;

  maxPrice: string;

  loading = false;

  params = new Filter();


  constructor(private offerService: OfferService,
              private categoryService: CategoryService) {

  }

  ngOnInit() {
    this.offerService.getAll()
      .subscribe(data => {
        this.offers = data;
      });

    this.categoryService.getAll()
      .subscribe(data => {
        this.categories = data;
        console.log(data);
      });


    timer(1000).subscribe(val => {
      this.selectedCategory = new Category();
      this.minPrice = '';
      this.maxPrice = '';

      this.selectedTags = null;

      this.loading = true;
    });


  }

  addMinPrice() {
    console.log('add min price: ' + this.minPrice);
    this.params.minPrice = this.minPrice;
  }

  addMaxPrice() {
    console.log('add max price: ' + this.maxPrice);
    this.params.maxPrice = this.maxPrice;
  }

  addCategory() {
    this.categoryService.getCategory(this.selectedCategory.id).subscribe(data => {
      this.selectedCategory = data as Category;
    });

    timer(100).subscribe(() => {
      this.params.category = this.selectedCategory.name;
    });

  }

  addTags() {
    const tags = this.selectedTags.split(', ');
    this.params.tags = tags;
    console.log(tags);
  }

  filter() {
    console.log('this.params: ' + JSON.stringify(this.params));

    this.offerService.getOffersByFilter(this.params).subscribe(
      data => {
        this.offers = data;
      }, error1 => {
        console.log('error');
        alert('not valid params');
      }
    );


    this.params = new Filter();
  }

  cancel() {
    this.offerService.getAll()
      .subscribe(data => {
        this.offers = data;
      });

    this.selectedCategory = new Category();

    this.minPrice = '';
    this.maxPrice = '';

    this.selectedTags = null;
  }


}
