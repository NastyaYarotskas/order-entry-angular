import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Offer} from '../../shared/models/offer';
import {OrderService} from '../../shared/order/order.service';
import {OrderReal} from '../../shared/models/OrderReal';
import {OrderItem} from '../../shared/models/order-item';

@Component({
  selector: 'app-shopping-cart-details',
  templateUrl: './shopping-cart-details.component.html',
  styleUrls: ['./shopping-cart-details.component.css']
})
export class ShoppingCartDetailsComponent implements OnInit {

  order: OrderReal;
  orderItems: OrderItem[];

  loading = false;

  constructor(private route: ActivatedRoute,
              public orderService: OrderService) {
  }

  ngOnInit() {
    this.getOrder();
  }

  getOrder(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.orderService.getOrderById(+id)
      .subscribe(order => {
        this.order = order as OrderReal;
        console.log('this.order: ' + order)
        this.orderItems = this.order.orderItems
        console.log('this.order.orderItems: ' + JSON.stringify(this.orderItems))
        this.loading = true;
      });
  }
}
