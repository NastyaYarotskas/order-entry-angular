import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8083/api/v1/customers';

  constructor(private http: HttpClient) { }

  getUser(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  getUserByEmail(email: string): Observable<Object> {
    return this.http.get(`${this.baseUrl}/authenticate/${email}`);
  }

  createUser(user: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, user);
  }

  updateUser(user: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}`, user);
  }

  getUsers(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  deleteUser(id: number): void {
    this.http.delete(`${this.baseUrl}/${id}`);
  }
}
