import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private baseUrl = '//localhost:8081/api/v1/categories';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    console.log(`${this.baseUrl}`);
    return this.http.get(`${this.baseUrl}`);
  }

  getCategory(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
}
