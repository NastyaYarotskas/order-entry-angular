export class Addres {
  city: string;
  id: number;
  street: string;

  constructor(
    city: string,
    street: string
  ) {
    this.street = street;
    this.city = city;
    this.id = 0;
  }

}
