export class OrderItem {
  id: number;
  name: string;
  quantity: number;
  offerId: number;
  price: number;
  category: string;
  orderId: number;
  tags: string[];
  imageUrl: string;
  description: string;

  constructor(
    name: string,
    quantity: number,
    offerId: number,
    price: number,
    category: string,
    orderId: number,
  ) {
    this.id = 0;
    this.name = name;
    this.quantity = quantity;
    this.offerId = offerId;
    this.price = price;
    this.category = category;
    this.orderId = orderId;
  }
}
