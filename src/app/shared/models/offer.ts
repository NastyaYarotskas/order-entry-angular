export class Offer {
  id: number;
  name: string;
  price: number;
  category: string;
  tags: string[];
}
