import {OrderItem} from './order-item';
import {Addres} from './addres';

export class OrderReal {
  id: number;

  orderItems: OrderItem[];

  status: string;

  deliveryAddress: Addres;

  customerId: number;

  totalPrice: number;

  orderItemCount: number;

  date: string;

  isPaid: boolean;

  email: string;
}
