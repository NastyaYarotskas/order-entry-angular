import {Addres} from './addres';

export class Order {
  id: number;
  email: string;
  customerId: number;
  deliveryAddress: Addres;

  constructor(
    customerId: number,
    deliveryAddress: Addres,
    email: string
  ) {
    this.customerId = customerId;
    this.email = email;
    this.deliveryAddress = deliveryAddress;
    this.id = 0;
  }
}
