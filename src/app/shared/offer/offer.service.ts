import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {interval, Observable, timer} from 'rxjs';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';
import {map, max} from 'rxjs/operators';
import {Filter} from '../models/filter';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  private baseUrl = '//localhost:8081/api/v1/offers';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getOffer(id: string): Observable<Object> {
    console.log(`${this.baseUrl}/${id}`);
    return this.http.get(`${this.baseUrl}/${+id}`);
  }

  getOffersByFilter(filter: Filter): Observable<any> {
    let params = new HttpParams();
    if (filter.category) {
      params = params.set('category', filter.category);
    }
    if (filter.minPrice) {
      params = params.set('minPrice', filter.minPrice);
    }
    if (filter.maxPrice) {
      params = params.set('maxPrice', filter.maxPrice);
    }
    if (filter.tags) {
      params = params.set('tags', filter.tags.toString());
    }
    // params.set('category', 'Books')
    console.log('params: ' + params);
    // params = filter;
    return this.http.get(`${this.baseUrl}/filter`, {params: params});


  }
}
