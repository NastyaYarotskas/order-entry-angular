import {Injectable} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {Order} from '../models/order';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, timer} from 'rxjs';
import {Offer} from '../models/offer';
import {OrderItem} from '../models/order-item';
import {Addres} from '../models/addres';
import {load} from '@angular/core/src/render3';
import {PayForm} from '../models/payForm';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private baseUrl = 'http://localhost:8080/api/v1/processor';

  private orderId = -1;

  private orderEmail = '';

  private _timer = timer(1000);

  constructor(private authService: AuthService,
              private http: HttpClient) {
  }

  createOrder() {
    const user = this.authService.getCurrentUser();
    const addres = new Addres('Minsk', 'Oktyabrskaya');
    const order = new Order(user.id, addres, user.email);
    this.orderEmail = user.email;
    return this.http.post(`${this.baseUrl}/order`, order).subscribe(data => {
      console.log('create order: ' + JSON.stringify(data));
      const order1 = data as Order;
      console.log('id ' + order1.id);
      this.orderId = order1.id;
    });

  }

  async addOrderItem(offer: Offer, quantity: number) {
    this._timer.subscribe(vara => {
      const orderItem = new OrderItem(offer.name, quantity, offer.id, offer.price, offer.category, this.orderId);
      console.log('order item: ' + JSON.stringify(orderItem));
      console.log(`${this.baseUrl}/item`);
      return this.http.post(`${this.baseUrl}/item`, orderItem).subscribe(data => {
        console.log('data' + data);
      });
    });
  }

  get getOrderId() {
    return this.orderId;
  }

  getOrdersByEmail(email: string) {
    console.log(`${this.baseUrl}/email/${email}`);
    return this.http.get(`${this.baseUrl}/email/${email}`);
  }

  getOrderById(id: number) {
    console.log(`${this.baseUrl}/order/${id}`);
    return this.http.get(`${this.baseUrl}/order/${id}`);
  }

  payForOrder(id: string) {
    console.log(`${this.baseUrl}/pay`);
    this.orderId = -1;
    const pay = new PayForm();
    pay.orderId = +id;
    console.log('pay: ' + JSON.stringify(pay));
    return this.http.post(`${this.baseUrl}/pay`, pay).subscribe(data => {
      console.log(data);
    });
  }

  setOrderAddress(orderId: string, address: Addres) {
    console.log(`${this.baseUrl}/address/${+orderId}`);

    return this.http.post(`${this.baseUrl}/address/${+orderId}`, address).subscribe(data => {
      console.log(JSON.stringify(data));
    });
  }
}
