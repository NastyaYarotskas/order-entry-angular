import {Injectable} from '@angular/core';
import {User} from '../models/user';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private userService: UserService) {
  }

  createAccount(user: User) {
    console.log('create account' + JSON.stringify(user))
    return this.userService.createUser(user);
  }
}
