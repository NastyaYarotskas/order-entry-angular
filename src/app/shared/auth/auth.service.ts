import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {Router} from '@angular/router';
import {UserService} from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router,
              private userService: UserService) {
    // localStorage.removeItem('currentUser');
  }

  logIn(email: string) {
    console.log(email);
    console.log('logged');
    const currentUser = this.userService.getUserByEmail(email);
    currentUser.subscribe(data => {
      console.log('current user' + data);
      localStorage.setItem('currentUser', JSON.stringify(data));
    });
    console.log('logIn ' + localStorage.getItem('currentUser'));
  }

  logOut(): void {
    localStorage.removeItem('currentUser');
    console.log('log out');
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem('currentUser') != null) {
      return true;
    }
    return false;
  }

  getCurrentUser(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}
