import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatalogComponent } from './components/catalog/catalog.component';
import {OfferService} from './shared/offer/offer.service';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatToolbarModule
} from '@angular/material';
import { OfferDetailsComponent } from './components/offer-details/offer-details.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserAccountComponent } from './components/user-account/user-account.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { OfferFilterComponent } from './components/offer-filter/offer-filter.component';
import { ShoppingCartDetailsComponent } from './components/shopping-cart-details/shopping-cart-details.component';
import { CartCalculatorComponent } from './components/cart-calculater/cart-calculator.component';
import { CheckoutsComponent } from './components/checkouts/checkouts.component';


@NgModule({
  declarations: [
    AppComponent,
    CatalogComponent,
    OfferDetailsComponent,
    LoginComponent,
    RegisterComponent,
    UserAccountComponent,
    NavbarComponent,
    ShoppingCartComponent,
    OfferFilterComponent,
    ShoppingCartDetailsComponent,
    CartCalculatorComponent,
    CheckoutsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatChipsModule,
    MatIconModule,
    FormsModule
  ],
  providers: [OfferService],
  bootstrap: [AppComponent]
})
export class AppModule { }
